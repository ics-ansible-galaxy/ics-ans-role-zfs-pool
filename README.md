ics-ans-role-zfs-pool
=====================

Ansible role to create a zfs pool.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
zfs_pool_name: servers
zfs_list_main:
  - /dev/sdb
  - /dev/sdc
zfs_list_spare:
  - /dev/sdf
  - /dev/sdg
zfs_zil: "mirror /dev/sdh /dev/sdi"
zfs_l2arc: "/dev/sdj /dev/sdk"
zfs_ashift: 12
zfs_autoexpand: on
zfs_autoreplace: on
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-zfs-pool
```

License
-------

BSD 2-clause
